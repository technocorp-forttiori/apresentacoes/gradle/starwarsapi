package com.api.starwars.controller;

import com.api.starwars.controller.v1.StarWarsController;
import com.api.starwars.domain.Person;
import com.api.starwars.service.PersonService;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.standaloneSetup;
import static org.mockito.Mockito.when;

@WebMvcTest
public class StarWarsControllerTest {

    @Autowired
    StarWarsController starWarsController;

    @MockBean
    private PersonService personService;

    @BeforeEach
    public void setUp() {
        standaloneSetup(this.starWarsController);
    }

    @Test
    public void deveRetornarSucesso_QuandoBuscarPersonagem() {
        List<Person> personList = new ArrayList<>();

        var luck = Person.builder()
                .personid(1L)
                .birthYear("Teste")
                .eyeColor("Blue")
                .gender("male")
                .name("Luck")
                .hairColor("Yellow")
                .mass("75kg")
                .skinColor("white")
                .height("1.70")
                .build();
        personList.add(luck);

        when(this.personService.findById(1L)).thenReturn(luck.toDto());
        given()
                .accept(ContentType.JSON)
                .when()
                .get("/v1/starWars/{personId}", luck.getPersonid())
                .then()
                .statusCode(HttpStatus.CREATED.value());
    }

//    @Test
//    public void deveRetornarSucesso_QuandoBsucarAListaDePersonagens(){
//        List<Person> personList = new ArrayList<>();
//
//        var luck = Person.builder()
//                .personid(1L)
//                .birthYear("Teste")
//                .eyeColor("Blue")
//                .gender("male")
//                .name("Luck")
//                .hairColor("Yellow")
//                .mass("75kg")
//                .skinColor("white")
//                .height("1.70")
//                .build();
//        personList.add(luck);
//
//        when(this.personService.findAll(1,10)).thenReturn(personList);
//        given()
//                .accept(ContentType.JSON)
//                .when()
//                .get("/v1/starWars/")
//                .then()
//                .statusCode(HttpStatus.OK.value());
//    }
}
