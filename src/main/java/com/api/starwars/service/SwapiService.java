package com.api.starwars.service;

import com.api.starwars.domain.Person;
import com.api.starwars.domain.dto.SwapiResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class SwapiService {

    private RestTemplate restTemplate;

    public List<Person> getPessoa() {

        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<SwapiResponse> body = new HttpEntity<SwapiResponse>(headers);

        ResponseEntity<SwapiResponse> exchange = restTemplate.exchange("https://swapi.dev/api/people/",
                HttpMethod.GET, body, SwapiResponse.class);

        log.info("method=getPessoa enchange={}", exchange.getBody());


        return exchange.getBody().getResults().stream().map(map -> {
            return Person.builder().name(map.getName()).height(map.getHeight()).mass(map.getMass())
                    .hairColor(map.getHairColor()).skinColor(map.getSkinColor()).eyeColor(map.getEyeColor())
                    .birthYear(map.getBirthYear()).gender(map.getGender()).build();
        }).collect(Collectors.toList());
    }
}
