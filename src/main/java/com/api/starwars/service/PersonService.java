package com.api.starwars.service;

import com.api.starwars.domain.Person;
import com.api.starwars.domain.dto.request.PersonRequest;
import com.api.starwars.domain.dto.response.PersonResponse;
import com.api.starwars.repository.PersonRepository;
import com.api.starwars.validations.Message;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
@Validated
public class PersonService {

    private SwapiService swapiService;

    private PersonRepository personRepository;

    public PersonResponse save(@Valid PersonRequest request) {

        personRepository.findByName(request.getName()).ifPresent(p -> {
            throw Message.PERSON_EXIST.asBusinessException();
        });

        Person person = Person.of(request);

        personRepository.save(person);

        log.info("method=save personId={}", person.getPersonid());

        return person.toDto();

    }

    public PersonResponse update(@Valid PersonRequest request, Long id) {

        Person person = personRepository.findById(id).orElseThrow(Message.NOT_FOUND::asBusinessException);

        person.update(request);

        personRepository.update(person);

        log.info("method=update personId={}", person.getPersonid());

        return person.toDto();

    }

    public PersonResponse findByName(String name) {
        Person person = personRepository.findByName(name).orElseThrow(Message.NOT_FOUND::asBusinessException);

        log.info("method=findByName name={}", person.getName());

        return person.toDto();
    }

    public PersonResponse findById(Long id) {

        Person person = personRepository.findById(id).orElseThrow(Message.NOT_FOUND::asBusinessException);

        log.info("method=findById personId={}", id);

        return person.toDto();

    }

    public List<PersonResponse> findAll(int page, int limit) {

        List<Person> findAll = personRepository.findAll(page, limit);

        log.info("method=findAll page={} limit={}", page, limit);

        return Person.toList(findAll);
    }

    public void deleteById(Long id) {

        personRepository.findById(id).orElseThrow(Message.NOT_FOUND::asBusinessException);

        personRepository.deleteById(id);

        log.info("method=deleteById personId={}", id);

    }

    public void swapiSave() {

        List<Person> persons = swapiService.getPessoa();

        personRepository.saveSwapi(persons);

        log.info("method=swapiSave size={}", persons.size());

    }
}
