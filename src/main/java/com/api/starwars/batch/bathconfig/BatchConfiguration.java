package com.api.starwars.batch.bathconfig;

import com.api.starwars.batch.bathprocessing.PersonItemProcessor;
import com.api.starwars.domain.dto.request.PersonRequest;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
@AllArgsConstructor
@NoArgsConstructor
public class BatchConfiguration {


    public JobBuilderFactory jobBuilderFactory;

    public StepBuilderFactory stepBuilderFactory;

    DataSource dataSource;

    @Bean
    public FlatFileItemReader<PersonRequest> reader() {
        FlatFileItemReader<PersonRequest> reader = new FlatFileItemReader<PersonRequest>();
        reader.setResource(new ClassPathResource("persons.csv"));
        reader.setLineMapper(new DefaultLineMapper<PersonRequest>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames("name", "height","mass","hairColor","skinColor","eyeColor","birthYear","gender");
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<PersonRequest>() {{
                setTargetType(PersonRequest.class);
            }});
        }});
        return reader;
    }

    @Bean
    public PersonItemProcessor processor() {
        return new PersonItemProcessor();
    }

    @Bean
    public JdbcBatchItemWriter<PersonRequest> writer() {
        JdbcBatchItemWriter<PersonRequest> writer = new JdbcBatchItemWriter<>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
        writer.setSql("INSERT INTO TB_PERSONAGEM " + "(name,height,mass,hairColor,skinColor,eyeColor,birthYear,gender) "
                + "VALUES " + "(#{name}," + "#{height}," + "#{mass}," + "#{hairColor}," + "#{skinColor}," + "#{eyeColor},"
                + "#{birthYear}," + "#{gender})");
        writer.setDataSource(dataSource);
        return writer;
    }

    @Bean
    public Job importUserJob(JdbcBatchItemWriter<PersonRequest> listener){
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1(listener))
                .end()
                .build();
    }

    @Bean
    public Step step1(JdbcBatchItemWriter<PersonRequest> writer) {
        return stepBuilderFactory.get("step1")
                .<PersonRequest, PersonRequest> chunk(10)
                .reader(reader())
                .processor(processor())
                .writer(writer)
                .build();
    }
}
