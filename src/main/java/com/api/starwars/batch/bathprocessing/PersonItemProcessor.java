package com.api.starwars.batch.bathprocessing;

import com.api.starwars.domain.dto.request.PersonRequest;
import org.springframework.batch.item.ItemProcessor;

public class PersonItemProcessor implements ItemProcessor<PersonRequest,PersonRequest> {
    @Override
    public PersonRequest process(PersonRequest person) throws Exception {

        final PersonRequest transformedPerson = new PersonRequest(
                person.getName(),
                person.getHeight(),
                person.getMass(),
                person.getHairColor(),
                person.getSkinColor(),
                person.getEyeColor(),
                person.getBirthYear(),
                person.getGender()
        );

        return transformedPerson;
    }
}
