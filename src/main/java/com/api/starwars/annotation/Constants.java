package com.api.starwars.annotation;

public class Constants {

    public static final String LIST_ALL_SUMMARY = "Listar todos os personagens";
    public static final String LIST_ALL_DESCRIPTION = "Irá realizar uma listagem dos personagens.";

    public static final String PERSON_SEARCH_ID_SUMMARY = "Procurar por Id";
    public static final String PERSON_SEARCH_ID_DESCRIPTION = "Irá realizar uma busca de um personagem pelo Id";

    public static final String PERSON_SEARCH_NAME_SUMMARY = "Procurar pelo nome";
    public static final String PERSON_SEARCH_NAME_DESCRIPTION = "Irá realizar uma busca de um personagem pelo nome";

    public static final String PERSON_SAVE_SUMMARY = "Registrar um personagem";
    public static final String PERSON_SAVE_DESCRIPTION = "Irá cadastrar um novo personagem";

    public static final String PERSON_DELETE_SUMMARY = "Deletar um personagem";
    public static final String PERSON_DELETE_DESCRIPTION = "Irá deletar um personagem do banco";

    public static final String PERSON_UPDATE_SUMMARY = "Atualizar um personagem";
    public static final String PERSON_UPDATE_DESCRIPTION = "Irá fazer uma atualização de um personagem do banco";

}
