package com.api.starwars.annotation;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ApiResponses(value = {
        @ApiResponse(code = 204, message = "Deletado com sucesso"),
        @ApiResponse(code = 404, message = "Personagem não encontrado"),
        @ApiResponse(code = 500, message = "Sistema indisponível") })
@ApiOperation(value = Constants.PERSON_DELETE_SUMMARY, notes = Constants.PERSON_DELETE_DESCRIPTION)
public @interface PersonDeleteCodeStandard {

}
