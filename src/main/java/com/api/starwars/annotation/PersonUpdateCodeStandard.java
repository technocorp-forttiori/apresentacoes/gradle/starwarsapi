package com.api.starwars.annotation;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD, ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ApiResponses(value = {
        @ApiResponse(code = 202, message = "Faz atualização de um personagem"),
        @ApiResponse(code = 404, message = "Personagem não encontrado"),
        @ApiResponse(code = 500, message = "Sistema indisponível") })
@ApiOperation(value = Constants.PERSON_UPDATE_SUMMARY, notes = Constants.PERSON_UPDATE_DESCRIPTION)
public @interface PersonUpdateCodeStandard {
}
