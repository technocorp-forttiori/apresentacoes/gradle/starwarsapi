package com.api.starwars.annotation;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD, ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "Retorna uma página com personagens"),
        @ApiResponse(code = 500, message = "Sistema indisponível") })
@ApiOperation(value = Constants.LIST_ALL_SUMMARY, notes = Constants.LIST_ALL_DESCRIPTION)
public @interface PersonListAllCodeStandard {
}
