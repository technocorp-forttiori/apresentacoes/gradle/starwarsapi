package com.api.starwars.domain;

import com.api.starwars.domain.dto.request.PersonRequest;
import com.api.starwars.domain.dto.response.PersonResponse;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Table(name = "TB_PERSONAGEM")
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long personid;

    private String name;

    private String height;

    private String mass;

    private String hairColor;

    private String skinColor;

    private String eyeColor;

    private String birthYear;

    private String gender;

    public static Person of(PersonRequest request) {
        return Person.builder().name(request.getName()).height(request.getHeight()).mass(request.getMass())
                .hairColor(request.getHairColor()).skinColor(request.getSkinColor()).eyeColor(request.getEyeColor())
                .birthYear(request.getBirthYear()).gender(request.getGender()).build();
    }

    public PersonResponse toDto() {
        return PersonResponse.builder().personid(this.personid).name(this.name).height(this.height).mass(this.mass)
                .hairColor(this.hairColor).skinColor(this.skinColor).eyeColor(this.eyeColor).birthYear(this.birthYear)
                .gender(this.gender).build();
    }

    public static List<PersonResponse> toList(List<Person> findAll) {

        return findAll.stream().map(m -> {

            return PersonResponse.builder().personid(m.personid).name(m.name).height(m.height).mass(m.mass)
                    .hairColor(m.hairColor).skinColor(m.skinColor).eyeColor(m.eyeColor).birthYear(m.birthYear)
                    .gender(m.gender).build();
        }).collect(Collectors.toList());

    }

    public void update(PersonRequest request){
        this.name=request.getName();
        this.height=request.getHeight();
        this.mass=request.getMass();
        this.hairColor=request.getHairColor();
        this.skinColor=request.getSkinColor();
        this.eyeColor=request.getEyeColor();
        this.birthYear=request.getBirthYear();
    }

}
