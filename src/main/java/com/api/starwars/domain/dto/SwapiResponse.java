package com.api.starwars.domain.dto;

import com.api.starwars.domain.dto.response.PersonSwapiResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class SwapiResponse {

    private List<PersonSwapiResponse> results;
}
