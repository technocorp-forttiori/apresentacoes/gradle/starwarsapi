package com.api.starwars.domain.dto.request;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class PersonRequest {

    @NotNull(message = "O campo name está inválido")
    @NotBlank(message = "O campo name está inválido")
    private String name;

    @NotNull(message = "O campo height está inválido")
    @NotBlank(message = "O campo height está inválido")
    private String height;

    @NotNull(message = "O campo mass está inválido")
    @NotBlank(message = "O campo mass está inválido")
    private String mass;

    @NotNull(message = "O campo hairColor está inválido")
    @NotBlank(message = "O campo hairColor está inválido")
    private String hairColor;

    @NotNull(message = "O campo skinColor está inválido")
    @NotBlank(message = "O campo skinColor está inválido")
    private String skinColor;

    @NotNull(message = "O campo eyeColor está inválido")
    @NotBlank(message = "O campo eyeColor está inválido")
    private String eyeColor;

    @NotNull(message = "O campo birthYear está inválido")
    @NotBlank(message = "O campo birthYear está inválido")
    private String birthYear;

    @NotNull(message = "O campo gender está inválido")
    @NotBlank(message = "O campo gender está inválido")
    private String gender;
}
