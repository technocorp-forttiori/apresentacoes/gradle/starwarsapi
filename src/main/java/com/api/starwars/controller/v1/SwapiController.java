package com.api.starwars.controller.v1;

import com.api.starwars.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/v1/swapi")
@RestController
@AllArgsConstructor
public class SwapiController {

    private final PersonService service;

    @GetMapping
    public ResponseEntity<Void> saveAll() {

        service.swapiSave();

        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
