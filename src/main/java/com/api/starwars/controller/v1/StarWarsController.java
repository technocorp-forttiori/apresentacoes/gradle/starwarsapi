package com.api.starwars.controller.v1;

import com.api.starwars.annotation.*;
import com.api.starwars.domain.dto.request.PersonRequest;
import com.api.starwars.domain.dto.response.PersonResponse;
import com.api.starwars.service.PersonService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.extern.java.Log;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

@RequestMapping("/v1/starWars")
@AllArgsConstructor
@RestController
@CrossOrigin("*")
@Builder
public class StarWarsController {

    private PersonService service;


    //@Scheduled(fixedRate = 5000) // Criando Jobs- Spring Scheduler <<<
    public void add2DBJob() {
        var person = PersonRequest.builder()
                .name("person" + new Random().nextInt())
                .eyeColor("eyeColor" + new Random().nextInt())
                .gender("Gender" + new Random().nextInt())
                .hairColor("Gender" + new Random().nextInt())
                .birthYear("birthYear" + new Random().nextInt())
                .mass("mass" + new Random().nextInt())
                .height("height" + new Random().nextInt())
                .skinColor("skinColor" + new Random().nextInt())
                .build();
        service.save(person);
    }

    @PostMapping
    @PersonSaveCodeStandard
    @ResponseStatus(HttpStatus.CREATED)
    public PersonResponse createPerson(@RequestBody PersonRequest request) {
        return service.save(request);
    }

    @PutMapping("/edit/{id}")
    @PersonUpdateCodeStandard
    @ResponseStatus(HttpStatus.ACCEPTED)
    public PersonResponse updatePerson(@RequestBody PersonRequest request, @PathVariable("id") Long personId) {
        return service.update(request, personId);
    }

    @GetMapping("/name/{name}")
    @PersonGetIdNameCodeStandard
    @ResponseStatus(HttpStatus.OK)
    public PersonResponse findByName(@PathVariable("name") String name) throws Exception {
        return service.findByName(name);
    }

    @GetMapping("/{id}")
    @PersonGetIdCodeStandard
    @ResponseStatus(HttpStatus.OK)
    public PersonResponse findByPerson(@PathVariable("id") Long personId) {
        return service.findById(personId);
    }

    @DeleteMapping("/{id}")
    @PersonDeleteCodeStandard
    public ResponseEntity<Void> deletePerson(@PathVariable("id") Long personId) {
        this.service.deleteById(personId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping
    @PersonListAllCodeStandard
    @ResponseStatus(HttpStatus.OK)
    public List<PersonResponse> findAll(
            @RequestParam(required = false, value = "page", defaultValue = "0") int page,
            @RequestParam(required = false, value = "limit", defaultValue = "30") int limit) {
        return service.findAll(page, limit);
    }
}
