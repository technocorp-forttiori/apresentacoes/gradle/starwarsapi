package com.api.starwars.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket productApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.api.starwars"))
                .paths(regex("/v1/starWars.*"))
                .build()
                .apiInfo(metaInfo());
    }

    private ApiInfo metaInfo(){
        ApiInfo apiInfo = new ApiInfo(
                "StarWars API REST",
                "API REST de Cadastro de Personagens.",
                "1.0",
                "Terms of Service",
                new Contact("Matheus Otto", "https://www.google.com.br",
                        "matheusotto@live.com"),
                "Apache License Version 2.0",
                "www.apache.org/licesen.html", new ArrayList<>()
        );
        return apiInfo;
    }
}
