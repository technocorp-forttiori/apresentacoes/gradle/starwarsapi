package com.api.starwars.repository;

import com.api.starwars.domain.Person;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface PersonRepository {

    @Insert("INSERT INTO TB_PERSONAGEM " + "(name,height,mass,hairColor,skinColor,eyeColor,birthYear,gender) "
            + "VALUES " + "(#{name}," + "#{height}," + "#{mass}," + "#{hairColor}," + "#{skinColor}," + "#{eyeColor},"
            + "#{birthYear}," + "#{gender})")
    @Options(keyColumn = "personid", keyProperty = "personid", useGeneratedKeys = true)
    Integer save(Person pessoa);

    default void saveSwapi(@Param("person") List<Person> items) {
        for (Person item : items) {
            save(item);
        }
    }

    @Select("SELECT * FROM TB_PERSONAGEM WHERE name=#{name}")
    Optional<Person> findByName(String name);

    @Select("SELECT * FROM TB_PERSONAGEM WHERE personid=#{personid}")
    Optional<Person> findById(Long personid);

    @Select("SELECT * FROM TB_PERSONAGEM LIMIT #{limit} offset #{page}")
    List<Person> findAll(int page, int limit);

    @Delete("DELETE FROM TB_PERSONAGEM WHERE personid=#{personid}")
    void deleteById(Long personid);

    @Update("UPDATE TB_PERSONAGEM SET name=#{name},height=#{height},mass=#{mass},hairColor=#{hairColor},skinColor=#{skinColor},eyeColor=#{eyeColor},birthYear=#{birthYear},gender=#{gender} WHERE personid=#{personid}")
    void update(Person person);
}
